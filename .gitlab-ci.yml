#
# Global Variables for Project and Service Name
# This could be replaced by predefined CI/CD Variables like CI_PROJECT_NAME and CI_PROJECT_PATH_SLUG
# if this reflects a proper naming like projectgroup/my-service
#
# We also need some CI/CD Variables on Project/Group level
# - DT_API_TOKEN (Dynatrace API Token)
# - DT_TENANT_ID (Dynatrace Tenant ID)
# - DT_PROBLEMS (bool if Dynatrace API should be used to detect problems)
# - KEPTN_API_TOKEN (keptn API Token)
# - KEPTN_BRIDGE (keptn Bridge URL for Deeplinks)
# - KEPTN_ENDPOINT (keptn API URL)
#
variables:
  PROJECT_NAME: gl-demo
  SERVICE_NAME: keptn-demo
  DOMAIN: glkptn.app


# Base Image to use for the keptn tasks
# - keptn cli
# - kubectl
# - helm3
# https://gitlab.com/everyonecancontribute/keptn/keptn-docker
image: registry.gitlab.com/everyonecancontribute/keptn/keptn-docker:6368dfd0820e89d7fd2d31b8883e0429225bbec3

#
# Including templates
# keptn-templates repository:
# https://gitlab.com/everyonecancontribute/keptn/keptn-templates
#
include:
  - project: 'everyonecancontribute/keptn/keptn-templates'
    ref: master
    file: '/config/onboarding.yml'
  - project: 'everyonecancontribute/keptn/keptn-templates'
    ref: master
    file: '/deployment/deployment-finished.yml'
  - project: 'everyonecancontribute/keptn/keptn-templates'
    ref: master
    file: '/qualitygates/qualitygate.yml'
  # Enable later for debuggin purpose
  # - template: Container-Scanning.gitlab-ci.yml
  # - template: SAST.gitlab-ci.yml
  # - template: Dependency-Scanning.gitlab-ci.yml
  # - template: License-Scanning.gitlab-ci.yml  
  # - template: Code-Quality.gitlab-ci.yml
  # - local: '.gitlab-unit-tests.yml'

#
# Define stages.
# The test stage is needed to include the default GitLab CI/CD Templates
#
stages:
    - build
    - test
    - keptn-onboarding        # Create or Update the keptn project
    - deploy-staging          # Deploy to a monitored environment
    - quality-gate-staging    # Run SLI/SLO Validation
    - deploy-production       # Deploy to Production when the Quality Gates are passed
    - quality-gate-production # Re evaluate the Production deployment

#
# Build development version of our Image
#
build-image:
  stage: build
  image: docker:latest
  services:
    - docker:dind
  before_script:
    - echo "$KUBECONFIG"
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - docker build --pull -t "$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA" .
    - docker push "$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA"
  except:
    - tags
  tags:
  - docker

# After the build, the GitLab templates for Container Scanning/Code Quality and SAST will be executed
#
# When all Tests are done, the onboarding of the project will be started on the next stage
# https://gitlab.com/everyonecancontribute/keptn/keptn-templates/-/tree/master/config
# This will create the project and service, and will upload all keptn resources within the keptn folder
# - https://gitlab.com/checkelmann/keptn-demo/-/tree/master/keptn (resources)
# - https://keptn.kdns.me/bridge/project/demo (created service on the bridge)



keptn-onboarding:
  stage: keptn-onboarding
  extends: .keptn-onboarding  # This will trigger the keptn onboarding workflow
  script:
  - echo "Keptn Onboarding ${PROJECT_NAME} - ${SERVICE_NAME} finished"

#
# Deploy staging environment
#
# https://gitlab.com/everyonecancontribute/keptn/keptn-templates/-/blob/master/deployment/deployment-finished.yml
# 
# Keptn will based on the deployment finished event execute some other tasks
# - Add metadata informations about the deployment to dynatrace
#     https://mep87209.live.dynatrace.com/#newservices/serviceOverview;id=SERVICE-9F70D7DD9125A690;gtf=-2h;gf=all
# - Create a Synthetic Monitor for my Application
#     https://mep87209.live.dynatrace.com/#httpcheckdetails;id=HTTP_CHECK-0934A31B559F68D1;gtf=-2h;gf=all
# - Trigger another GitLab Pipeline
#     https://gitlab.com/checkelmann/keptn-gitlab-service/-/pipelines
# - Execute JMeter tests
#     https://keptn.kdns.me/bridge/project/demo
# - Evaluate the deployment with the defined SLI/SLOs (see keptn folder)
#
deploy-staging:
  stage: deploy-staging
  extends: .send-deployment-finished  # This will trigger the keptn workflow
  environment: 
    name: staging
  variables:
    TEST_STRATEGY: "performance"
    DEPLOYMENT_URI_PUBLIC: https://${CI_PROJECT_NAME}-${CI_ENVIRONMENT_SLUG}.${DOMAIN}
    IMAGE: "$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA"
    IMAGE_TAG: $CI_COMMIT_SHA
    DEPLOYMENT_VERION: $CI_COMMIT_SHA
  script:
  - kubectl create ns ${CI_ENVIRONMENT_NAME} || /bin/true
  - kubectl annotate ns ${CI_ENVIRONMENT_NAME} app.gitlab.com/app=$CI_PROJECT_PATH_SLUG
  - kubectl annotate ns ${CI_ENVIRONMENT_NAME} app.gitlab.com/env=$CI_ENVIRONMENT_SLUG
  - helm upgrade --install --debug -i ${CI_PROJECT_NAME}
      --set image.tag=${CI_COMMIT_SHA}
      --set unleash.instanceid=${FF_INSTANCEID}
      --set unleash.url=${CI_API_V4_URL}/feature_flags/unleash/${CI_PROJECT_ID}
      --set unleash.appname=${CI_PROJECT_NAME}
      --set ingress.hosts[0].host=${CI_PROJECT_NAME}-${CI_ENVIRONMENT_SLUG}.${DOMAIN}
      --set ingress.hosts[0].paths[0]=/
      --set ingress.tls[0].secretName=${CI_PROJECT_NAME}-${CI_ENVIRONMENT_SLUG}.${DOMAIN}
      --set ingress.tls[0].hosts[0]=${CI_PROJECT_NAME}-${CI_ENVIRONMENT_SLUG}.${DOMAIN}
      --set ci_project_name=${PROJECT_NAME}
      --set ci_service_name=${CI_PROJECT_NAME}
      --set ci_environment_slug=${CI_ENVIRONMENT_SLUG}
      -n ${CI_ENVIRONMENT_NAME} ./charts/${CI_PROJECT_NAME}

#
# KEPTN QUALITY GATES
# https://gitlab.com/everyonecancontribute/keptn/keptn-templates/-/blob/master/qualitygates/qualitygate.yml
#
# With this job, we will get the results from the SLI/SLO Evaluation and stopping the pipeline
# if the defined SLO Criterias are not met
#

qualitygate-staging:
  stage: quality-gate-staging
  variables:
    ENVIRONMENT: staging
  extends: .keptn_evaluation
  needs: ["deploy-staging"]

#
# Add telegram notification to staging on a deployment finished event
# keptn add-resource --project=demo --service=keptn-demo --stage=staging --resource=__deployment.finished.http --resourceUri=generic-executor/deployment.finished.http
#

#
# Deploy production environment
#
# deploy-production:
#   stage: deploy-production
#   extends: .send-deployment-finished
#   environment: 
#     name: production
#   variables:
#     TEST_STRATEGY: "performance"
#     DEPLOYMENT_URI_PUBLIC: https://${CI_PROJECT_NAME}-${CI_ENVIRONMENT_SLUG}.${DOMAIN}
#     IMAGE: "$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA"
#     IMAGE_TAG: $CI_COMMIT_SHA
#     DEPLOYMENT_VERION: $CI_COMMIT_SHA
#   script: 
#   - helm upgrade --install --create-namespace --debug -i ${CI_PROJECT_NAME}
#       --set image.tag=${CI_COMMIT_SHA}
#       --set unleash.instanceid=${FF_INSTANCEID}
#       --set unleash.url=${CI_API_V4_URL}/feature_flags/unleash/${CI_PROJECT_ID}
#       --set unleash.appname=${CI_PROJECT_NAME}
#       --set ingress.hosts[0].host=${CI_PROJECT_NAME}-${CI_ENVIRONMENT_SLUG}.${DOMAIN}
#       --set ingress.hosts[0].paths[0]=/
#       --set ingress.tls[0].secretName=${CI_PROJECT_NAME}-${CI_ENVIRONMENT_SLUG}.${DOMAIN}
#       --set ingress.tls[0].hosts[0]=${CI_PROJECT_NAME}-${CI_ENVIRONMENT_SLUG}.${DOMAIN}
#       --set ci_project_name=${PROJECT_NAME}
#       --set ci_service_name=${CI_PROJECT_NAME}
#       --set ci_environment_slug=${CI_ENVIRONMENT_SLUG}
#       --set gitlab.env=$CI_ENVIRONMENT_SLUG ¿
#       -n ${CI_ENVIRONMENT_NAME} ./charts/${CI_PROJECT_NAME}

# qualitygate-production:
#   stage: quality-gate-production
#   variables:
#     ENVIRONMENT: production
#   extends: .keptn_evaluation
#   needs: ["deploy-production"]
      

